﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public class ToggleControl : MonoBehaviour
    {

        public GameObject bigSister;
        public GameObject littleSister;
        public bool bigSisterOn = true;

        void Start()
        {
            //find the spawned player character
            bigSister = GameObject.Find("Big Sister(Clone)") as GameObject;
        }

        void Update()
        {

            if (Input.GetButtonDown("Toggle"))
            {
                Debug.Log("ButtonPushed");
                bigSisterOn = !bigSisterOn;

                if (bigSisterOn)
                {
                    Debug.Log("Big Sister On");
                    //set Big Sister to Player
                    bigSister.tag = "Player";
                    bigSister.layer = 6;
                    bigSister.GetComponent<AIFollower>().enabled = false;

                    //Set Little sister to Follow
                    littleSister.tag = "Untagged";
                    littleSister.layer = 0;
                    littleSister.GetComponent<AIFollower>().enabled = true;

                }
                else
                {
                    Debug.Log("Little Sister On");
                    //set Little Sister to Player
                    littleSister.tag = "Player";
                    littleSister.layer = 6;
                    bigSister.GetComponent<AIFollower>().enabled = true;

                    //Set Big sister to Follow
                    bigSister.tag = "Untagged";
                    bigSister.layer = 0;
                    littleSister.GetComponent<AIFollower>().enabled = false;
                }

            }
        }
    }
}
